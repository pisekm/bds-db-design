-- -----------------------------------------------------
-- Table public.contact
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.contact (
  contact_id SERIAL NOT NULL,
  phone_number VARCHAR(45) NOT NULL,
  email VARCHAR(45) NULL,
  PRIMARY KEY (contact_id));

-- -----------------------------------------------------
-- Table public.insurance_company
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.insurance_company (
  insurance_company_id SERIAL NOT NULL,
  insurance_company VARCHAR(45) NOT NULL,
  insurance_company_code INTEGER NOT NULL,
  PRIMARY KEY (insurance_company_id));

-- -----------------------------------------------------
-- Table public.patient
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.patient (
  patient_id SERIAL NOT NULL UNIQUE,
  name VARCHAR(45) NOT NULL,
  surname VARCHAR(45) NOT NULL,
  date_of_birth DATE NOT NULL,
  personal_no VARCHAR(45) NOT NULL,
  contact_id SERIAL NOT NULL,
  insurance_company_id INTEGER NOT NULL,
  PRIMARY KEY (patient_id, contact_id, insurance_company_id),
  CONSTRAINT fk_patient_contact1
    FOREIGN KEY (contact_id)
    REFERENCES public.contact (contact_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_patient_insurance_company1
    FOREIGN KEY (insurance_company_id)
    REFERENCES public.insurance_company (insurance_company_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.doctor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.doctor (
  doctor_id SERIAL NOT NULL UNIQUE,
  doctor_name VARCHAR(45) NOT NULL,
  doctor_surname VARCHAR(45) NOT NULL,
  contact_id SERIAL NOT NULL,
  PRIMARY KEY (doctor_id, contact_id),
  CONSTRAINT fk_doctor_contact1
    FOREIGN KEY (contact_id)
    REFERENCES public.contact (contact_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.patient_has_doctor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.patient_has_doctor (
  patient_id SERIAL NOT NULL,
  doctor_id SERIAL NOT NULL,
  PRIMARY KEY (patient_id, doctor_id),
  CONSTRAINT fk_patient_has_doctor_patient1
    FOREIGN KEY (patient_id)
    REFERENCES public.patient (patient_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_patient_has_doctor_doctor1
    FOREIGN KEY (doctor_id)
    REFERENCES public.doctor (doctor_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.diagnose
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.diagnose (
  diagnose_id SERIAL NOT NULL,
  diagnose VARCHAR(45) NOT NULL,
  PRIMARY KEY (diagnose_id));

-- -----------------------------------------------------
-- Table public.patient_has_diagnose
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.patient_has_diagnose (
  patient_id SERIAL NOT NULL,
  diagnose_id SERIAL NOT NULL,
  PRIMARY KEY (diagnose_id, patient_id),
  CONSTRAINT fk_patient_has_diagnose_patient1
    FOREIGN KEY (patient_id)
    REFERENCES public.patient (patient_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_patient_has_diagnose_diagnose1
    FOREIGN KEY (diagnose_id)
    REFERENCES public.diagnose (diagnose_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.department
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.department (
  department_id SERIAL NOT NULL,
  department VARCHAR(45) NOT NULL,
  floor INTEGER NULL,
  PRIMARY KEY (department_id));

-- -----------------------------------------------------
-- Table public.patient_in_department
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.patient_in_department (
  patient_id SERIAL NOT NULL,
  department_id SERIAL NOT NULL,
  date DATE NOT NULL,
  PRIMARY KEY (patient_id, department_id),
  CONSTRAINT fk_patient_has_department_patient1
    FOREIGN KEY (patient_id)
    REFERENCES public.patient (patient_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_patient_has_department_department1
    FOREIGN KEY (department_id)
    REFERENCES public.department (department_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.address
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.address (
  address_id SERIAL NOT NULL,
  city VARCHAR(45) NOT NULL,
  street VARCHAR(45) NOT NULL,
  house_number VARCHAR(45) NOT NULL,
  zip_code INTEGER NOT NULL,
  PRIMARY KEY (address_id));

-- -----------------------------------------------------
-- Table public.patient_has_address
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.patient_has_address (
  patient_id SERIAL NOT NULL,
  address_id SERIAL NOT NULL,
  PRIMARY KEY (patient_id, address_id),
  CONSTRAINT fk_patient_has_address_patient1
    FOREIGN KEY (patient_id)
    REFERENCES public.patient (patient_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_patient_has_address_address1
    FOREIGN KEY (address_id)
    REFERENCES public.address (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.hospital
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.hospital (
  hospital_id SERIAL NOT NULL UNIQUE,
  hospital_name VARCHAR(45) NOT NULL,
  address_id SERIAL NOT NULL,
  PRIMARY KEY (hospital_id, address_id),
  CONSTRAINT fk_hospital_address1
    FOREIGN KEY (address_id)
    REFERENCES public.address (address_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table public.hospital_has_patient
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS public.hospital_has_patient (
  hospital_id SERIAL NOT NULL,
  patient_id SERIAL NOT NULL,
  PRIMARY KEY (hospital_id, patient_id),
  CONSTRAINT fk_hospital_has_patient_hospital1
    FOREIGN KEY (hospital_id)
    REFERENCES public.hospital (hospital_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_hospital_has_patient_patient1
    FOREIGN KEY (patient_id)
    REFERENCES public.patient (patient_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);